#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexique.h"
#include "lexunits.h"
#include "syntax.h"
#include "affiche_arbre_abstrait.h"
#include "dico.h"

int nb_ligne = 1;

int main(int argc, char **argv) {
    int uniteCourante;
    yyin = fopen(argv[1], "r");
    if(yyin == NULL){
        fprintf(stderr, "impossible d'ouvrir le fichier %s\n", argv[1]);
        exit(1);
    }

    lexinit();
    n_prog* programme = prog();
    affiche_n_prog(programme);
    //affiche_dico();

    return 0;
}

