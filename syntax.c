#include <stdio.h>
#include <stdlib.h>


#include "lexique.h"
#include "lexunits.h"
#include "syntax.h"
#include "syntabs.h"

extern char yytext[100];
extern int nb_ligne;

n_prog *prog(){          /* FINI */
    n_l_dec *n1 = NULL, *n2 = NULL;
    n_prog *prog;

    cc = yylex();

    if (cc == ENTIER){
		n1 = optDecVar();
	}
	if (cc == ID_FCT){
		n2 = lstDecFonc();
	}

    //cc = yylex();
	if (cc == '.')
        return cree_n_prog(n1, n2);
	else
        erreur("Ne se termine pas par un '.'");
}

/**--------------------------------------
        LISTE DECLARATION
#-----------------------------------------*/

n_l_dec *optDecVar(){
    //n_l_dec n1 = NULL ;
    if (cc == ENTIER){
		 return lstDecVar();
		if (cc == ';'){
			cc = yylex();
		}
		else erreur(yytext);
	}

}

n_l_dec *lstDecVar(){
    n_dec *ndec = NULL;
    n_l_dec *nldec = NULL;

    if (cc == ENTIER){
		ndec = decVar();

    //nldec = lstDecVarB();
		if (cc == ','){
			nldec = lstDecVarB();
		}
	}
	else{
        erreur(yytext);
	}

    return cree_n_l_dec(ndec , nldec);
}

n_l_dec *lstDecVarB(){
    n_dec *n1 = NULL;
    n_l_dec *n2 = NULL;

    if (cc == ','){
		cc = yylex();
		if (cc == ENTIER){
			n1 = decVar();
			if (cc == ','){
				n2 = lstDecVarB();
			}
			//if(cc == ';')// yylex();
                return cree_n_l_dec(n1, n2);
            //else
            //    erreur(yytext);
		}else
			erreur(yytext);
	}
	return NULL;
}

n_dec *decVar(){
    char* txt;
    unsigned int tailleTab = 0;

    if (cc == ENTIER){
		cc = yylex();
		if (cc == ID_VAR){
            //fprintf(stderr,"yytext=[%s]\n",yytext);
            txt = strdup(yytext);

			cc = yylex();
			if (cc == '['){
				tailleTab = optTailleTab();
				cc = yylex();
				return cree_n_dec_tab(txt, tailleTab);
			}
			return cree_n_dec_var(txt);
		}
		else
			erreur(yytext);
	}
	else
		erreur(yytext);
}

int optTailleTab(){
    int *taille = 0;
   if (cc == '['){
		cc = yylex();
		if (cc == NOMBRE){
            taille = atoi(yytext);
			cc = yylex();
			if (cc == ']'){
				cc = yylex();
			}
			else
				erreur(yytext);
            return taille;
		}
		else
			erreur(yytext);
	}
}

n_l_dec *lstDecFonc(){
    n_dec *ndec = NULL;
    n_l_dec *nldec = NULL;

   if (cc == ID_FCT){
		ndec = decFonc();

		//cc = yylex();
		if (cc == ID_FCT){
			nldec = lstDecFonc();
		}
	}

	return cree_n_l_dec(ndec, nldec);
}

n_dec *decFonc(){
    n_l_dec *nldecParam = NULL;
    n_l_dec *nldecOpt = NULL;
    n_instr *ninstr = NULL;
    char* nom;

    if (cc == ID_FCT){
        nom = calloc(strlen(yytext), sizeof(char));
        strncpy(nom, yytext, strlen(yytext));
		cc = yylex();
		if (cc == '('){
			nldecParam = lstParam();
			if (cc == ENTIER){
				nldecOpt = optDecVar();
				cc = yylex();
			}

            //cc = yylex();
			if (cc == '{'){
				ninstr = instrBloc();
			}
			else erreur(yytext);

            return cree_n_dec_fonc(nom, nldecParam, nldecOpt, ninstr);

		}else
			erreur(yytext);

	}else
		erreur(yytext);
}

n_l_dec *lstParam(){
    n_l_dec *nlLstDecVar = NULL;
    if (cc == '('){
		cc = yylex();
		if (cc == ENTIER){
			nlLstDecVar = optLstDecVar();
		}
		if (cc == ')'){
			cc = yylex();
		}
		else erreur(yytext);

		return nlLstDecVar;
	}
	else
		erreur(yytext);
}

n_l_dec *optLstDecVar(){
   if (cc == ENTIER){
		return lstDecVar();
	}
}

/**-------------------------------------
        FIN LISTE DECLARATION
#----------------------------------------*/

/**-----------------------------------------
        INSTRUCTIONS
#------------------------------------------*/

n_instr *inst(){
	if (cc == ID_VAR){
		return instrAffect();
	}
	else if (cc == '{'){
		return instrBloc();
	}
	else if (cc == SI){
		return instrSi();
	}
	else if (cc == TANTQUE){
		return instrTantque();
	}
	else if (cc == ID_FCT){
		return instrAppel();
	}
	else if (cc == RETOUR){
		return instrRetour();
	}
	else if (cc == ECRIRE){
		return instrEcrire();
	}
	else if (cc == POUR){
		return instrPour();
	}
	else if (cc == ';'){
		return instrVide();
	}
	else
		erreur(yytext);
}

n_instr *instrAffect(){
    n_var *nvar = NULL;
    n_exp *nexp = NULL;
	if (cc == ID_VAR){
		nvar = var();
		if (cc == '='){
			cc = yylex();
			if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
				nexp = exp();
				if (cc == ';'){
					cc = yylex();
				}
				else erreur(yytext);

                return cree_n_instr_affect(nvar,nexp);
			}
			else
				erreur(yytext);
		}
		else
			erreur(yytext);
	}
	else
		erreur(yytext);
}

n_instr *instrBloc(){
    n_l_instr *nliste = NULL;

    if(cc != '{')
		erreur(yytext);

    cc = yylex();
	nliste = listeInstr();

    if(cc != '}')
		erreur(yytext);

    cc = yylex();
    return cree_n_instr_bloc(nliste);
	/*if (cc == '{'){
		cc = yylex();
		if (cc == ID_VAR || cc == SI || cc == TANTQUE || cc == ECRIRE || cc == RETOUR
			|| cc == ID_FCT || cc == '{' || cc == ';'){
			listeInstr();
		}
		if (cc == '}'){
			cc = yylex();
		}
		else
			erreur(yytext);
	}
	else
		erreur(yytext);*/
}

n_l_instr *listeInstr(){
    n_instr *ninst = NULL;
    n_l_instr *nlinstr = NULL;

	if (cc == ID_VAR || cc == SI || cc == TANTQUE || cc == ECRIRE || cc == RETOUR
		|| cc == ID_FCT || cc == '{' || cc == ';'){
		ninst = inst();
		if (cc == ID_VAR || cc == SI || cc == TANTQUE || cc == ECRIRE || cc == RETOUR
			|| cc == ID_FCT || cc == '{' || cc == ';'){
			nlinstr = listeInstr();
		}
		return cree_n_l_instr(ninst, nlinstr);
	}
}

n_instr *instrAppel(){
    n_instr *ninst = NULL;

	if (cc == ID_FCT){
		ninst = appelFct();
		if (cc == ';'){
			cc = yylex();
		}
		else erreur(yytext);

        return cree_n_instr_appel(ninst);
	}
	else
		erreur(yytext);
}

n_instr *instrSi(){     /*FINI*/
    n_instr *S0 = NULL;
    n_exp *S2 = NULL;
    n_instr *S4 = NULL;
    n_instr *S6 = NULL;

    if(cc != SI)
        erreur("IF attendu");

    cc = yylex();
    S2 = exp();

    if(cc != ALORS)
        erreur("THEN attendu");

    cc = yylex();
    S4 = instrBloc();

    if(cc == SINON){
        S6 = optSinon();
    }

    S0 = cree_n_instr_si(S2, S4, S6);
    return S0;

}

n_instr *optSinon(){
    n_instr *S1 = NULL;

    if (cc != SINON){
        erreur(yytext);
    }
    cc = yylex();
	S1 = instrBloc();
	return S1;
	/*if (cc == ELSE){
		cc = yylex();
		if (cc == '{'){
			instrBloc();
		}
	}*/
}

n_instr *instrTantque(){
    n_exp *S2;
	n_instr *S4;

	//printf("<instrTantque>\n");

	if(cc != TANTQUE)
		erreur(yytext);

	cc=yylex();
	S2 = exp();

    if(cc != FAIRE)
		erreur(yytext);

    cc=yylex();
    S4=instrBloc();
    //printf("</instrTantque>\n");

	return cree_n_instr_tantque(S2,S4);

	/*if (cc == TANTQUE){
		cc = yylex();
		if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
			exp();
			if (cc == FAIRE){
				cc = yylex();
				if (cc == '{'){
					instrBloc();
				}
				else
					erreur(yytext);
			}
			else
				erreur(yytext);
		}
		else
			erreur(yytext);
	}
	else
		erreur(yytext);*/
}

n_instr *instrRetour(){
    n_exp *nexp = NULL;

    if (cc != RETOUR){
        erreur(yytext);
    }

    cc = yylex();
    nexp = exp();

    if (cc != ';'){
        erreur(yytext);
    }

	cc = yylex();

	return cree_n_instr_retour(nexp);
}

n_instr *instrEcrire(){
    n_exp *nexp = NULL;

	if (cc == ECRIRE){
		cc = yylex();
		if (cc == '('){
			cc = yylex();
			if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
				nexp = exp();
				if (cc == ')'){
					cc = yylex();
					if (cc == ';'){
						cc = yylex();
					}
					else erreur(yytext);

					return cree_n_instr_ecrire(nexp);
				}
				else
					erreur(yytext);
			}
			else
				erreur(yytext);
		}
		else
			erreur(yytext);
	}
	else
		erreur(yytext);
}

n_instr *instrPour(){
    n_instr *naffect1, *naffect2, *nblocinstr;
    n_exp *nexp;

    if(cc == POUR){
        cc = yylex();

        if(cc != ID_VAR)
            erreur(yytext);

        naffect1 = instrAffect();

        if(cc == ';')
            cc = yylex();
        else erreur(yytext);

        nexp = exp();

        if(cc == ';')
            cc = yylex();
        else erreur(yytext);

        naffect2 = instrAffect();

        if(cc == '{')
            nblocinstr = instrBloc();
        else erreur(yytext);

        return cree_n_instr_pour(naffect1, nexp, naffect2, nblocinstr);
    }else erreur(yytext);

}

n_instr *instrVide(){
	if (cc == ';'){
		cc = yylex();
	}
	else{
        erreur(yytext);
	}

    return cree_n_instr_retour(NULL);
}


/**-------------------------
        FIN INSTRUCTIONS
#-------------------------*/

/**-------------------------
        EXPRESSION
#-------------------------*/

n_exp *exp(){
    n_exp *n1 = NULL, *n2 = NULL;

    if ( cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE ){
        n1=conj();
        if (cc == '|'){
			n2 = optOuExp();
            return cree_n_exp_op(ou,n1,n2) ;
		}
		else return n1;
    }
    else erreur(yytext);

}

n_exp *optOuExp(){
	cc = yylex();

	if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE)
		return exp();
	else erreur(yytext);
}

n_exp *conj(){
    n_exp *n1 = NULL;
    n_exp *n2 = NULL;

	if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		n1 = neg();
		if (cc == '&'){
			n2 = optEtConj();
			return cree_n_exp_op(et, n1, n2);
		}
		return n1;
	}
	else erreur(yytext);
}

n_exp *optEtConj(){
	cc = yylex();

	if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE)
		return conj();
	else erreur(yytext);
}

n_exp *neg(){
    n_exp *ncomp = NULL;

    int isneg = 0;
	if (cc == '!'){
        cc = yylex();
        isneg = 1;
	}

	if (cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		ncomp = comp();
		if (isneg){
            return cree_n_exp_op(non, ncomp, NULL);
		}
		return ncomp;
	}
	else erreur(yytext);
}

n_exp *comp(){
    n_exp *nexpArith = NULL;
    n_exp *nComp = NULL;

    //fprintf(stderr,"debug: a yytext=[%s]\n",yytext);

	if (cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nexpArith = expArith();
		if (cc == '=' || cc == '<'){
            char c = cc;
			nComp = compB();
			if(c == '='){
                return cree_n_exp_op(egal, nexpArith, nComp);
			}else{
			    return cree_n_exp_op(inf, nexpArith, nComp);
			}
		}
		else return nexpArith;
	}
	else erreur(yytext);
}

n_exp *compB(){
    n_exp *nexpArith = NULL;
    n_exp *nComp = NULL;
	cc = yylex();
	if (cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nexpArith = expArith();
		if (cc == '=' || cc == '<'){
            char c = cc;
			nComp = compB();
			if(c == '='){
                return cree_n_exp_op(egal, nexpArith, nComp);
			}else{
			    return cree_n_exp_op(inf, nexpArith, nComp);
			}
		}
		else return nexpArith;
	}
	else erreur(yytext);
}

n_exp *expArith(){
    n_exp *nterme = NULL;
    n_exp *nexpA = NULL;

	if (cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nterme = terme();
		if (cc == '+' || cc == '-'){
            char c = cc;
			nexpA = expArithB();
            if(c == '+'){
                return cree_n_exp_op(plus, nterme, nexpA);
			}else{
			    return cree_n_exp_op(moins, nterme, nexpA);
			}
		}
		else return nterme;
	}
	else erreur(yytext);
}

n_exp *expArithB(){
    n_exp *nterme = NULL;
    n_exp *nexpA = NULL;

	cc = yylex();

	if (cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nterme = terme();
		if (cc == '+' || cc == '-'){
			char c = cc;
			nexpA = expArithB();
            if(c == '+'){
                return cree_n_exp_op(plus, nterme, nexpA);
			}else{
			    return cree_n_exp_op(moins, nterme, nexpA);
			}
		}
		else return nterme;
	}
	else erreur(yytext);
}

n_exp *terme(){
    n_exp *nfacteur = NULL;
    n_exp *nterme = NULL;

	if (cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nfacteur =  facteur();
		if (cc == '*' || cc == '/'){
            char c = cc;
			nterme = termeB();
            if(c == '*'){
                return cree_n_exp_op(fois, nfacteur, nterme);
            }else{
                return cree_n_exp_op(divise, nfacteur, nterme);
            }
		}
		else return nfacteur;
	}
	else erreur(yytext);
}

n_exp *termeB(){
    n_exp *nfacteur = NULL;
    n_exp *nterme = NULL;

    cc = yylex();

	if (cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nfacteur =  facteur();
		if (cc == '*' || cc == '/'){
            char c = cc;
			nterme = termeB();
            if(c == '*'){
                return cree_n_exp_op(fois, nfacteur, nterme);
            }else{
                return cree_n_exp_op(divise, nfacteur, nterme);
            }
		}
		else return nfacteur;
	}
	else erreur(yytext);
}
n_exp *facteur(){
        //fprintf(stderr,"debug: facteur yytext=[%s]\n",yytext);

	if (cc == '('){
        n_exp *nexp = NULL;
		cc = yylex();

		if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
			nexp = exp();
		}
		else erreur(yytext);
		if (cc == ')'){
			cc = yylex();
		}
		else erreur(yytext);

        return nexp;

	}else if (cc == NOMBRE){
	    int i;
	    i=atoi(yytext);
		cc = yylex();
		return cree_n_exp_entier(i);

	}else if (cc == ID_FCT){
		return cree_n_appel(yytext, appelFct());

	}else if (cc == ID_VAR){
		return cree_n_exp_var(var());

	}else if (cc == LIRE){
		cc = yylex();
		if (cc == '('){
			cc = yylex();
			if (cc == ')')
				cc = yylex();
			else erreur(yytext);

            return cree_n_exp_lire();

		}else erreur(yytext);

	}else erreur(yytext);
}

/**-------------------------
        FIN EXPRESSION
#-------------------------*/


/**-------------------------
        VARIABLE
#-------------------------*/

n_var *var(){
    char* nom;
    //memset(nom, '\0', 100);
	if (cc == ID_VAR){
	    //strcpy(nom, yytext);
	    nom = strdup(yytext);
		cc = yylex();
		if (cc == '['){
            n_exp *nind = optInd();
            return cree_n_var_indicee(nom, nind);
		}
	}
	else erreur(yytext);

    return cree_n_var_simple(nom);

}

n_exp *optInd(){
    n_exp *nexp = NULL;

	cc = yylex();

	if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nexp = exp();
		if (cc == ']'){
			cc = yylex();
		}
		else erreur(yytext);
	}
	else erreur(yytext);

	return nexp;
}

n_appel *appelFct(){
    n_l_exp *nlexp = NULL;
    char* nom;

	if (cc == ID_FCT){
        nom = calloc(strlen(yytext), sizeof(char));
        strncpy(nom, yytext, strlen(yytext));
		cc = yylex();
		if (cc == '('){
			cc = yylex();
			if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
				nlexp = lstExp();
			}
			if (cc == ')'){
				cc = yylex();
			}
			else erreur(yytext);

            return cree_n_appel(nom, nlexp);

		}
		else erreur(yytext);
	}
	else erreur(yytext);
}

n_l_exp *lstExp(){
    n_exp *nexp = NULL;
    n_l_exp *nlexp = NULL;

	if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
		nexp = exp();
		if (cc == ','){
			nlexp = lstExpB();
		}
		return cree_n_l_exp(nexp, nlexp);
	}else{
        erreur(yytext);
	}
}

n_l_exp *lstExpB(){
    n_exp *nexp = NULL;
    n_l_exp *nlexp = NULL;

	if (cc == ','){
		cc = yylex();
		if (cc == '!' || cc == '(' || cc == NOMBRE || cc == ID_VAR || cc == ID_FCT || cc == LIRE){
            nexp = exp();
		if (cc == ','){
			nlexp = lstExpB();
		}
		return cree_n_l_exp(nexp, nlexp);
        }
		else erreur(yytext);
	}
}

/**-------------------------
        FIN VARIABLE
#-------------------------*/

