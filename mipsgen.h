#define MIPS_DATA_SIZE  32
#define MIPS_DATA_MAX   32
#define MIPS_TEXT_SIZE  32
#define MIPS_TEXT_MAX   32
#define MIPS_LINE_SIZE  32
#define MIPS_MAXLEN     4096


typedef struct instrline{
    char* instr;
    struct instrline* nextline;
} instrline;

typedef struct{
    FILE* f;
    char** data;
    unsigned int data_c;
    char** text;
    unsigned int text_c;
    char code[MIPS_MAXLEN];

    //instrline* firstline;
    //instrline* lastline;
} mipsfile;

mipsfile* open_mips_file(const char* fname);
void write_mips_file_and_quit(mipsfile* f);

void mips_append_line(mipsfile* f, const char* line);
void mips_add_data(mipsfile* f, const char* dat);
void mips_add_text(mipsfile* f, const char* txt);
