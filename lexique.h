#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define xstr(s) str(s)
#define str(s) #s

#define LEX_SIZE    512
#define LEX_MAXLEN  32

char yytext[100];
FILE *yyin;

int yylex();
void lexinit();
unsigned char issymbol(const char c);
unsigned char iskeyword(const char* s, unsigned int *rkwtype);
unsigned char isvar(const char* c);
unsigned char isfunc(const char* s);
unsigned char isnum(const char* s);

char **lex;
