#include "syntabs.h"

int cc;

n_prog *prog();

n_l_dec *optDecVar();
n_l_dec *lstDecVar();
n_l_dec *lstDecVarB();
n_dec *decVar();
int optTailleTab();
n_l_dec *lstDecFonc();
n_dec *decFonc();
n_l_dec *lstParam();
n_l_dec *optLstDecVar();

n_instr *inst();
n_instr *instrAppel();
n_instr *instrBloc();
n_l_instr *listeInstr();
n_instr *instrAffect();
n_instr *instrSi();
n_instr *optSinon();
n_instr *instrTantque();
n_instr *instrRetour();
n_instr *instrEcrire();
n_instr *instrPour();
n_instr *instrVide();

n_exp *exp();
n_exp *optOuExp();
n_exp *conj();
n_exp *optEtConj();
n_exp *neg();
n_exp *comp();
n_exp *compB();
n_exp *expArith();
n_exp *expArithB();
n_exp *terme();
n_exp *termeB();
n_exp *facteur();

n_var *var();
n_exp *optInd();
n_appel *appelFct();
n_l_exp *lstExp();
n_l_exp *lstExpB();
