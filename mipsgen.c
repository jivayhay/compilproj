#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "mipsgen.h"

mipsfile* open_mips_file(const char* fname){
    mipsfile* f = (mipsfile*) malloc(sizeof(mipsfile));
    if(f == NULL)
        erreur("creation fichier");

    f->f = fopen(fname, "w");
    if(f->f == NULL)
        erreur("erreur ouverture fichier");

    //f->firstline = NULL;
    //f->lastline = NULL;

    f->data = (char **) calloc(MIPS_DATA_MAX, MIPS_DATA_SIZE * sizeof(char));
    for(int i = 0; i < MIPS_DATA_MAX ; i++){
        f->data[i] = calloc(MIPS_DATA_SIZE, sizeof(char));
    }

    f->text = (char **) calloc(MIPS_TEXT_MAX, MIPS_TEXT_SIZE * sizeof(char));
    for(int i = 0; i < MIPS_TEXT_MAX ; i++){
        f->text[i] = calloc(MIPS_TEXT_SIZE, sizeof(char));
    }

    f->data_c = 0;
    f->text_c = 0;

    return f;
}

/*void appendmipsline(mipsfile* f, const char* line){
    instrline* newline = (instrline*) malloc(sizeof(instrline));

    if(newline == NULL)
        erreur("malloc instr line");

    strcpy(newline->instr, line);
    newline->nextline = NULL;

    if(f->firstline != NULL){
        f->lastline->nextline = newline;
        f->lastline = newline;
    }else{
        f->firstline = newline;
        f->lastline = newline;
    }
}*/

void mips_append_line(mipsfile* f, const char* line){
    sprintf(f->code, "%s\n%s", f->code, line);
}

void mips_add_data(mipsfile* f, const char* dat){
    strcpy(f->data[f->data_c], dat);
    f->data_c++;
}


void mips_add_text(mipsfile* f, const char* txt){
    strcpy(f->text[f->data_c], txt);
    f->text_c++;
}

void write_mips_file_and_quit(mipsfile* f){
    printf(".data\n");
    fprintf(f->f, ".data\n");
    for(int i = 0 ; i < f->data_c ; i++){
        printf("%s\n", f->data[i]);
        fprintf(f->f, "%s\n", f->data[i]);
    }

    printf(".text\n");
    fprintf(f->f, ".text\n");

    printf("__start:\n\njal main\nli $v0, 10\nsyscall\n");
    fprintf(f->f, "__start:\n\njal main\nli $v0, 10\nsyscall\n");

    for(int i = 0 ; i < f->text_c ; i++){
        printf("%s\n", f->text[i]);
        fprintf(f->f, "%s\n", f->text[i]);
    }

    printf("%s", f->code);
    fprintf(f->f, "%s", f->code);

    fclose(f->f);

}
