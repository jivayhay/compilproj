/* Codes des mots-clefs */
#define SI      301   // si
#define ALORS   302   // alors
#define SINON   303   // sinon
#define TANTQUE 304   // tantque
#define FAIRE   305   // faire
#define ENTIER  306   // entier
#define RETOUR  307   // retour
#define LIRE    308   // lire
#define ECRIRE  309   // ecrire
#define POUR    310   // pour

/* Codes des types identificateurs et nombres */
#define ID_VAR  352    // \$[0-9a-zA-Z_\$]*
#define ID_FCT  353    // [a-zA-Z_][0-9a-zA-Z_\$]*
#define NOMBRE  351    // [0-9]+

