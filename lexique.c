#include "lexique.h"
#include "lexunits.h"

extern int nb_ligne;

int yylex(){
    memset(yytext, '\0', 100);
    char curchar;
    char* keyword = calloc(LEX_MAXLEN, sizeof(char));

    int i;
    curchar = fgetc(yyin);

    if(curchar == '#'){
        while(curchar != '\n')
            curchar = fgetc(yyin);
    }

    while(curchar == ' ' || curchar == '\n'){
        if(curchar == '\n')
            nb_ligne++;
        curchar = fgetc(yyin);
    }

    if(issymbol(curchar)) return curchar;

    for(i = 0; (curchar != '\n') && (curchar != ' ') ; i++){
        keyword[i] = yytext[i] = curchar;

        /*
        if(curchar == '('){
            do{
                keyword[i] = yytext[i] = curchar;
                curchar = fgetc(yyin);
                i++;
            }while(curchar != ')');
            keyword[i] = yytext[i] = curchar;

            break;
        }
        */


        curchar = fgetc(yyin);

        if(issymbol(curchar)){
            if (ungetc(curchar, yyin) != curchar) exit(1);
            break;
        }
    }

    if(isnum(keyword)){
        free(keyword);
        return NOMBRE;
    }

    if(isvar(keyword)){
        free(keyword);
        return ID_VAR;
    }

    unsigned int kwtype;
    if(iskeyword(keyword, &kwtype)){
        free(keyword);
        return kwtype;
    }

    if(isfunc(keyword)){
        free(keyword);
        return ID_FCT;
    }

    char result = keyword[0];
    free(keyword);
    return result;
}


unsigned char issymbol(const char c){
    return (c == ',') || (c == ' ') || (c == '\n') || (c == ';') || (c == '{') || (c == '}') || (c == '=') || (c == '(') || (c == ')') || (c == '[') || (c == ']') || (c == '.') || (c == EOF) ;
}

unsigned char iskeyword(const char* s, unsigned int *rkwtype){
    int i=0;

    if(strncmp(s, "ecrire", 6) == 0){
        *rkwtype = ECRIRE;
        return 1;
    }
    if(strncmp(s, "lire", 4) == 0){
        *rkwtype = LIRE;
        return 1;
    }

    for(i = 0; i < LEX_SIZE ; i++){
        //printf("%d ", strlen(lex[i]));
        if(strncmp(s, lex[i], LEX_MAXLEN) == 0){
            *rkwtype = i;
            return 1;
        }
    }

    return 0;
}

unsigned char isvar(const char* s){
    char beginvarid;
    char varname[LEX_MAXLEN];
    int result = (sscanf(s, "%['$']%s", &beginvarid, varname) == 2);
    return result;
}

/**
TODO: verifier que le premier caractere ne soit pas un nombre puis continuer sur alphanumerique
*/
unsigned char isfunc(const char* s){
    char funcname[LEX_MAXLEN];
    int result = (sscanf(s, "%"xstr(LEX_MAXLEN)"[a-zA-Z]", funcname) == 1);
    return result;
}

unsigned char isnum(const char* s){
    int num = -1;
    unsigned char c;
    int result = (sscanf(s, "%d%c", &num, &c) == 1);
    return result;
}

/**
Vérifie que les caractères suivis par le caractère c soient des commentaires
d'après la syntaxe proposée pour les commentaires.
Replace le caractère c et les caractères suivants dans le flux pour qu'ils soient
interprétés plus tard par la skipcomment() par exemple
*/
unsigned char iscomment(const char c){
    if(c == '#'){
        ungetc(c, yyin);
        return 1;
    }else if(c == '/'){
        char nextc = fgetc(yyin);
        if ((nextc == '*') || (nextc == '/')){
            ungetc(nextc, yyin);
            ungetc(c, yyin);
            return 1;
        }else{
            ungetc(nextc, yyin);
            ungetc(c, yyin);
            return 0;
        }
    }
}

/**
TODO: vérifier EOF
*/
void skipcomment(){
    char c = fgetc(yyin);

    if(c == '#'){
        while(c != '\n')
            c = fgetc(yyin);

    }else if(c == '/'){
        c = fgetc(yyin);
        if(c == '/')
            while(c != '\n')
                c = fgetc(yyin);

        else if(c == '*'){
            while(c != '*'){
                c = fgetc(yyin);
                if(c == '*'){
                    char nextc = fgetc(yyin);
                    if(nextc != '/'){
                        ungetc(nextc, yyin);
                        c = nextc;
                    }
                }
            }
        }
    }
}

void lexinit(){
    lex = (char **)calloc(LEX_SIZE, LEX_MAXLEN * sizeof(char));
    for(int i = 0; i < LEX_SIZE ; i++)
        lex[i] = (char *)calloc(LEX_MAXLEN, sizeof(char));

    lex[301] = "si";
    lex[302] = "alors";
    lex[303] = "sinon";
    lex[304] = "tantque";
    lex[305] = "faire";
    lex[306] = "entier";
    lex[307] = "retour";
    lex[308] = "lire";
    lex[309] = "ecrire";
    lex[310] = "pour";

    lex[351] = "var";
    lex[352] = "fct";
    lex[353] = "num";
}
