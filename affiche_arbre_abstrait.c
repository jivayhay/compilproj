#include <stdio.h>
#include "syntabs.h"
#include "util.h"
#include "dico.h"
#include "mipsgen.h"

extern int contexte;
extern int adresseLocaleCourante;
extern int adresseArgumentCourant;

void affiche_n_prog(n_prog *n);
void affiche_l_instr(n_l_instr *n);
void affiche_instr(n_instr *n);
void affiche_instr_si(n_instr *n);
void affiche_instr_tantque(n_instr *n);
void affiche_instr_affect(n_instr *n);
void affiche_instr_appel(n_instr *n);
void affiche_instr_retour(n_instr *n);
void affiche_instr_ecrire(n_instr *n);
void affiche_l_exp(n_l_exp *n);
unsigned char affiche_exp(n_exp *n);
unsigned char affiche_varExp(n_exp *n);
unsigned char affiche_opExp(n_exp *n);
unsigned char affiche_intExp(n_exp *n);
unsigned char affiche_lireExp(n_exp *n);
unsigned char affiche_appelExp(n_exp *n);
void affiche_l_dec(n_l_dec *n);
void affiche_dec(n_dec *n);
void affiche_foncDec(n_dec *n);
void affiche_varDec(n_dec *n);
void affiche_tabDec(n_dec *n);
void affiche_var(n_var *n);
void affiche_var_simple(n_var *n);
void affiche_var_indicee(n_var *n);
void affiche_appel(n_appel *n);

unsigned int get_offset(char* var);
void empile(char *reg);
void empile(char *reg);

int trace_abs = 1;

char* var_names[maxDico];
unsigned int var_names_i = 0;
unsigned int next_flag = 0;

unsigned char next_reg = 0;

mipsfile* mips;

/*-------------------------------------------------------------------------*/

void affiche_n_prog(n_prog *n)
{
  mips = open_mips_file("toast.mips");

  char *fct = "prog";
  affiche_balise_ouvrante(fct, trace_abs);

  contexte = C_VARIABLE_GLOBALE;

  affiche_l_dec(n->variables);
  affiche_l_dec(n->fonctions);
  affiche_balise_fermante(fct, trace_abs);

  printf("\n\n");
  write_mips_file_and_quit(mips);
}

/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/

void affiche_l_instr(n_l_instr *n)
{
  char *fct = "l_instr";
  if(n){
  affiche_balise_ouvrante(fct, trace_abs);
    affiche_instr(n->tete);
    affiche_l_instr(n->queue);
  affiche_balise_fermante(fct, trace_abs);
  }
}

/*-------------------------------------------------------------------------*/

void affiche_instr(n_instr *n)
{
  if(n){
    if(n->type == blocInst) affiche_l_instr(n->u.liste);
    else if(n->type == affecteInst) affiche_instr_affect(n);
    else if(n->type == siInst) affiche_instr_si(n);
    else if(n->type == tantqueInst) affiche_instr_tantque(n);
    else if(n->type == appelInst) affiche_instr_appel(n);
    else if(n->type == retourInst) affiche_instr_retour(n);
    else if(n->type == ecrireInst) affiche_instr_ecrire(n);
  }
}

/*-------------------------------------------------------------------------*/

void affiche_instr_si(n_instr *n)
{
  char *fct = "instr_si";
  affiche_balise_ouvrante(fct, trace_abs);

  affiche_exp(n->u.si_.test);
  affiche_instr(n->u.si_.alors);
  if(n->u.si_.sinon){
    affiche_instr(n->u.si_.sinon);
  }
  affiche_balise_fermante(fct, trace_abs);
}

/*-------------------------------------------------------------------------*/

void affiche_instr_tantque(n_instr *n)
{
  char *fct = "instr_tantque";
  affiche_balise_ouvrante(fct, trace_abs);

  affiche_exp(n->u.tantque_.test);
  affiche_instr(n->u.tantque_.faire);
  affiche_balise_fermante(fct, trace_abs);
}

/*-------------------------------------------------------------------------*/

void affiche_instr_affect(n_instr *n)
{
  if(rechercheExecutable(n->u.affecte_.var->nom) < 0)
    erreur("fonction non-declaree");

  char *fct = "instr_affect";
  affiche_balise_ouvrante(fct, trace_abs);


  affiche_var(n->u.affecte_.var);
  unsigned int result_reg = affiche_exp(n->u.affecte_.exp);
  affiche_balise_fermante(fct, trace_abs);

  char lineout[MIPS_LINE_SIZE];
  sprintf(lineout, "move \t$t%d \t$t%d", next_reg + 1, result_reg);
  mips_append_line(mips, lineout);

  //sprintf(lineout, "move \t%d \t$t%d", rechercheExecutable(n->u.affecte_.var->nom), next_reg);
  //mips_append_line(mips, lineout);
  next_reg++;
}

/*-------------------------------------------------------------------------*/

void affiche_instr_appel(n_instr *n)
{
  char *fct = "instr_appel";
  affiche_balise_ouvrante(fct, trace_abs);


  affiche_appel(n->u.appel);
  affiche_balise_fermante(fct, trace_abs);
}
/*-------------------------------------------------------------------------*/

void affiche_appel(n_appel *n)
{
  if(rechercheExecutable(n->fonction) < 0)
    erreur("fonction non-declaree");

  char *fct = "appel";
  affiche_balise_ouvrante(fct, trace_abs);
  affiche_texte( n->fonction, trace_abs);
  affiche_l_exp(n->args);
  affiche_balise_fermante(fct, trace_abs);
}

/*-------------------------------------------------------------------------*/

void affiche_instr_retour(n_instr *n)
{
  char *fct = "instr_retour";
  affiche_balise_ouvrante(fct, trace_abs);
  affiche_exp(n->u.retour_.expression);
  affiche_balise_fermante(fct, trace_abs);

}

/*-------------------------------------------------------------------------*/
//TODO : à vérifier (démerde toi quand tu relira ce code après t'être murgé au Pussy)
void affiche_instr_ecrire(n_instr *n)
{
  char *fct = "instr_ecrire";
  affiche_balise_ouvrante(fct, trace_abs);
  affiche_exp(n->u.ecrire_.expression);
  affiche_balise_fermante(fct, trace_abs);
}

/*-------------------------------------------------------------------------*/

void affiche_l_exp(n_l_exp *n)
{
  char *fct = "l_exp";
  affiche_balise_ouvrante(fct, trace_abs);

  if(n){
    affiche_exp(n->tete);
    affiche_l_exp(n->queue);
  }
  affiche_balise_fermante(fct, trace_abs);
}

/*-------------------------------------------------------------------------*/

unsigned char affiche_exp(n_exp *n)
{

  if(n->type == varExp) return affiche_varExp(n);
  else if(n->type == opExp) return affiche_opExp(n);
  else if(n->type == intExp) return affiche_intExp(n);
  else if(n->type == appelExp) return affiche_appelExp(n);
  else if(n->type == lireExp) return affiche_lireExp(n);
}

/*-------------------------------------------------------------------------*/

unsigned char affiche_varExp(n_exp *n)
{
  if(rechercheExecutable(n->u.var->nom) < 0)
    erreur("variable non-declaree");

  char *fct = "varExp";
  affiche_balise_ouvrante(fct, trace_abs);
  affiche_var(n->u.var);
  affiche_balise_fermante(fct, trace_abs);

  char lineout[MIPS_LINE_SIZE];
  sprintf(lineout, "la \t$t%d \t%s", next_reg, n->u.var->nom);
  mips_append_line(mips, lineout);
  sprintf(lineout, "lw \t$t%d \t%d($t%d)", next_reg+1, get_offset(n->u.var->nom), next_reg);
  mips_append_line(mips, lineout);
  sprintf(lineout, "move \t$t%d \t$t%d", next_reg, next_reg+1);
  mips_append_line(mips, lineout);
  next_reg++;

  return next_reg;
}

/*-------------------------------------------------------------------------*/
unsigned char affiche_opExp(n_exp *n)
{
  char *fct = "opExp";
  affiche_balise_ouvrante(fct, trace_abs);

  char lineout[MIPS_LINE_SIZE];
  if(n->u.opExp_.op == plus){
      affiche_texte("plus", trace_abs);
      sprintf(lineout, "add \t$t%d \t$t%d \t$t%d", next_reg - 2, next_reg - 2, next_reg -1);
      next_reg--;
      mips_append_line(mips, lineout);

  }else if(n->u.opExp_.op == moins){
      affiche_texte("moins", trace_abs);
      affiche_texte("plus", trace_abs);
      sprintf(lineout, "sub \t$t%d \t$t%d \t$t%d", next_reg - 2, next_reg - 2, next_reg -1);
      next_reg--;
      mips_append_line(mips, lineout);

  }else if(n->u.opExp_.op == fois){
      affiche_texte("fois", trace_abs);
      sprintf(lineout, "mult \t$t%d \t$t%d", next_reg - 2, next_reg -1);
      mips_append_line(mips, lineout);
      sprintf(lineout, "mflo \t$t%d", next_reg - 2);
      mips_append_line(mips, lineout);
      next_reg--;

  }else if(n->u.opExp_.op == divise){
      affiche_texte("divise", trace_abs);
      sprintf(lineout, "div \t$t%d \t$t%d", next_reg - 2, next_reg -1);
      mips_append_line(mips, lineout);
      sprintf(lineout, "mflo \t$t%d", next_reg - 2);
      mips_append_line(mips, lineout);
      next_reg--;

  }else if(n->u.opExp_.op == egal){
      affiche_texte("egal", trace_abs);
      sprintf(lineout, "li \t$t%d \t-2", next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "beq \t$t%d \t$t%d \te%d", next_reg - 2, next_reg - 1, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "addi \t$t%d \t$t%d 1", next_reg, next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d: \taddi \t$t%d \t$t%d 1", next_flag, next_reg, next_reg);
      mips_append_line(mips, lineout);
      next_flag++;
      sprintf(lineout, "move \t$t%d \t$t%d", next_reg - 2, next_reg);
      mips_append_line(mips, lineout);

      next_reg--;

  }else if(n->u.opExp_.op == diff){
      affiche_texte("diff", trace_abs);
      sprintf(lineout, "li \t$t%d \t-2", next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "bne \t$t%d \t$t%d \te%d", next_reg - 2, next_reg - 1, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "addi \t$t%d \t$t%d 1", next_reg, next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d: \taddi \t$t%d \t$t%d 1", next_flag, next_reg, next_reg);
      mips_append_line(mips, lineout);
      next_flag++;
      sprintf(lineout, "move \t$t%d \t$t%d", next_reg - 2, next_reg);
      mips_append_line(mips, lineout);

      next_reg--;
  }else if(n->u.opExp_.op == inf){
      affiche_texte("inf", trace_abs);
      sprintf(lineout, "li \t$t%d \t-2", next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "blt \t$t%d \t$t%d \te%d", next_reg - 2, next_reg - 1, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "addi \t$t%d \t$t%d 1", next_reg, next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d: \taddi \t$t%d \t$t%d 1", next_flag, next_reg, next_reg);
      mips_append_line(mips, lineout);
      next_flag++;
      sprintf(lineout, "move \t$t%d \t$t%d", next_reg - 2, next_reg);
      mips_append_line(mips, lineout);

      next_reg--;
  }else if(n->u.opExp_.op == infeg){
      affiche_texte("infeg", trace_abs);
      sprintf(lineout, "li \t$t%d \t-2", next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "ble \t$t%d \t$t%d \te%d", next_reg - 2, next_reg - 1, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "addi \t$t%d \t$t%d 1", next_reg, next_reg);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d: \taddi \t$t%d \t$t%d 1", next_flag, next_reg, next_reg);
      mips_append_line(mips, lineout);
      next_flag++;
      sprintf(lineout, "move \t$t%d \t$t%d", next_reg - 2, next_reg);
      mips_append_line(mips, lineout);

      next_reg--;
  }else if(n->u.opExp_.op == ou){
      affiche_texte("ou", trace_abs);
      sprintf(lineout, "bne \t$t%d \t$0 e%d", next_reg - 2, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "bne \t$t%d \t$0 e%d", next_reg - 1, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "li \t$t%d \t0", next_reg - 2);
      mips_append_line(mips, lineout);
      sprintf(lineout, "j \te%d", next_flag + 1);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d: \tli \t$t%d \t-1", next_flag, next_reg - 2);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d:", next_flag + 1);
      mips_append_line(mips, lineout);

      next_reg--;
      next_flag += 2;

  }else if(n->u.opExp_.op == et){
      affiche_texte("et", trace_abs);
      sprintf(lineout, "beq \t$t%d \t$0 e%d", next_reg - 2, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "beq \t$t%d \t$0 e%d", next_reg - 1, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "li \t$t%d \t-1", next_reg - 2);
      mips_append_line(mips, lineout);
      sprintf(lineout, "j \te%d", next_flag + 1);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d: \tli \t$t%d \t0", next_flag, next_reg - 2);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d:", next_flag + 1);
      mips_append_line(mips, lineout);

      next_reg--;
      next_flag += 2;

  }else if(n->u.opExp_.op == non){
      affiche_texte("non", trace_abs);
      sprintf(lineout, "bne \t$t%d \t$0 e%d", next_reg - 1, next_flag);
      mips_append_line(mips, lineout);
      sprintf(lineout, "li \t$t%d \t-1", next_reg - 1);
      mips_append_line(mips, lineout);
      sprintf(lineout, "j \te%d", next_flag + 1);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d: \tli \t$t%d \t0", next_flag, next_reg - 1);
      mips_append_line(mips, lineout);
      sprintf(lineout, "e%d:", next_flag + 1);
      mips_append_line(mips, lineout);

      next_flag += 2;
  }

  if( n->u.opExp_.op1 != NULL ) {
    affiche_exp(n->u.opExp_.op1);
  }
  if( n->u.opExp_.op2 != NULL ) {
    affiche_exp(n->u.opExp_.op2);
  }
  affiche_balise_fermante(fct, trace_abs);

  return next_reg;
}

/*-------------------------------------------------------------------------*/

unsigned char affiche_intExp(n_exp *n)
{
  char texte[ 50 ]; // Max. 50 chiffres
  sprintf(texte, "%d", n->u.entier);
  affiche_element( "intExp", texte, trace_abs );

  char lineout[MIPS_LINE_SIZE];
  sprintf(lineout, "li \t$t%d \t%d", next_reg, n->u.entier);
  mips_append_line(mips, lineout);
  //next_reg++;

  return next_reg;
}

/*-------------------------------------------------------------------------*/
unsigned char affiche_lireExp(n_exp *n)
{
  char *fct = "lireExp";
  affiche_balise_ouvrante(fct, trace_abs);
  affiche_balise_fermante(fct, trace_abs);

  char lineout[MIPS_LINE_SIZE];
  sprintf(lineout, "li \t$v0, \t5\n");
  mips_append_line(mips, lineout);
  sprintf(lineout, "syscall\n");
  mips_append_line(mips, lineout);
  sprintf(lineout, "move \t%d \t$v0\n", next_reg);
  mips_append_line(mips, lineout);

  next_reg++;

  return next_reg;
}

/*-------------------------------------------------------------------------*/

unsigned char affiche_appelExp(n_exp *n)
{
  char *fct = "appelExp";
  affiche_balise_ouvrante(fct, trace_abs);
  affiche_appel(n->u.appel);
  affiche_balise_fermante(fct, trace_abs);
}

/*-------------------------------------------------------------------------*/

void affiche_l_dec(n_l_dec *n)
{
  char *fct = "l_dec";

  if( n ){
    affiche_balise_ouvrante(fct, trace_abs);
    affiche_dec(n->tete);
    affiche_l_dec(n->queue);
    affiche_balise_fermante(fct, trace_abs);
  }
}

/*-------------------------------------------------------------------------*/

void affiche_dec(n_dec *n)
{

  if(n){
    if(n->type == foncDec) {
      affiche_foncDec(n);
    }
    else if(n->type == varDec) {
      affiche_varDec(n);
    }
    else if(n->type == tabDec) {
      affiche_tabDec(n);
    }
  }
}

/*-------------------------------------------------------------------------*/

void affiche_foncDec(n_dec *n)
{
  int foncDecArgC;
  n_l_dec *currentParam = n->u.foncDec_.param;

  if(rechercheDeclarative(n->nom) != -1)
    erreur("fonction deja declaree");

  char lineout[MIPS_LINE_SIZE];
  sprintf(lineout, "%s:", n->nom);

  for(foncDecArgC = 0 ; currentParam != NULL ; foncDecArgC++)
    currentParam = currentParam->queue;

  ajouteIdentificateur(n->nom, contexte, T_FONCTION, 0, foncDecArgC);

  affiche_dico();

  char *fct = "foncDec";
  affiche_balise_ouvrante(fct, trace_abs);
  entreeFonction();

  affiche_texte( n->nom, trace_abs );

  contexte = C_ARGUMENT;
  affiche_l_dec(n->u.foncDec_.param);

  contexte = C_VARIABLE_LOCALE;
  affiche_l_dec(n->u.foncDec_.variables);
  affiche_instr(n->u.foncDec_.corps);
  affiche_balise_fermante(fct, trace_abs);
  sortieFonction();
}

/*-------------------------------------------------------------------------*/

void affiche_varDec(n_dec *n)
{
  if(rechercheDeclarative(n->nom) != -1)
    erreur("variable deja declaree");

  if(contexte != C_ARGUMENT){
    ajouteIdentificateur(n->nom, contexte, T_ENTIER, adresseLocaleCourante, 0);
    adresseLocaleCourante += 1;
  }else{
    ajouteIdentificateur(n->nom, contexte, T_ENTIER, adresseArgumentCourant, 0);
    adresseArgumentCourant += 1;
  }

  if(contexte == C_VARIABLE_GLOBALE){
    char dat[MIPS_DATA_SIZE];
    sprintf(dat, "%s\t.word\n", n->nom);
    mips_add_data(mips, dat);
  }

  var_names[var_names_i] = n->nom;
  var_names_i++;

  affiche_dico();

  affiche_element("varDec", n->nom, trace_abs);
}

/*-------------------------------------------------------------------------*/

void affiche_tabDec(n_dec *n)
{
  if(rechercheDeclarative(n->nom) != -1)
    erreur("variable deja declaree");

  if(contexte != C_ARGUMENT){
    ajouteIdentificateur(n->nom, contexte, T_ENTIER, adresseLocaleCourante, n->u.tabDec_.taille);
    adresseLocaleCourante += 1 + n->u.tabDec_.taille;
  }else{
    ajouteIdentificateur(n->nom, contexte, T_ENTIER, adresseArgumentCourant, n->u.tabDec_.taille);
    adresseArgumentCourant += 1 + n->u.tabDec_.taille;
  }

  if(contexte == C_VARIABLE_GLOBALE){
    char dat[MIPS_DATA_SIZE];
    sprintf(dat, "%s\t.space\t%d\n", n->nom, n->u.tabDec_.taille);
    mips_add_data(mips, dat);
  }

  var_names[var_names_i] = n->nom;
  var_names_i++;

  affiche_dico();

  char texte[100]; // Max. 100 chars nom tab + taille
  sprintf(texte, "%s[%d]", n->nom, n->u.tabDec_.taille);
  affiche_element( "tabDec", texte, trace_abs );
}

/*-------------------------------------------------------------------------*/

void affiche_var(n_var *n)
{
  if(n->type == simple) {
    affiche_var_simple(n);
  }
  else if(n->type == indicee) {
    affiche_var_indicee(n);
  }
}

/*-------------------------------------------------------------------------*/
void affiche_var_simple(n_var *n)
{
  if(rechercheExecutable(n->nom) < 0)
    erreur("variable non-declaree");

  affiche_element("var_simple", n->nom, trace_abs);
}

/*-------------------------------------------------------------------------*/
void affiche_var_indicee(n_var *n)
{
  if(rechercheExecutable(n->nom) < 0)
    erreur("variable non-declaree");

  affiche_element("var_indicee", n->nom, trace_abs);

}
/*-------------------------------------------------------------------------*/

unsigned int get_offset(char* var){
	for (int i = 0; i < maxDico; ++i){
		if (strcmp(var, var_names[i]) == 0)
			return i * 4;
	}
	return -1;
}

void empile( char *reg ) {
    printf( "\tsubu\t$sp, $sp, 4\t# alloue un mot sur la pile\n" );
    printf( "\tsw\t%s, ($sp)\t# copie reg vers sommet de pile\n", reg );
}
void depile( char *reg ) {
    printf( "\tlw\t%s, ($sp)\t# copie sommet de pile vers reg\n", reg );
    printf( "\taddu\t$sp, $sp, 4\t# desalloue un mot sur la pile\n" );
}
